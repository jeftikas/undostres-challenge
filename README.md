Automation Engineer Proficiency Exercise
(API AUTOMATION TEST)

EL challenge test fue realizado con Cypress y Cucumber

En la carpeta e2e se encuentran dos carpetas: features y step_definitions

En la carpeta feature se encuentra el archivo api.feature que tiene la descripción de las características que se quieren probar y los casos de prueba asociados
En la carpeta step_definitions se encuentra el archivo api.js donde esta el codigo de los escenarios de pruebas

Para correr el proyecto utilize el comando:

npm run cypress:open


