import { Given, When } from "@badeball/cypress-cucumber-preprocessor";
const url = "https://reqres.in";
const newuser = {
  name: "morpheus",
  job: "leader",
};
const updateuser = {
  name: "morpheus",
  job: "zion resident",
};
const correctemail = {
  email: "eve.holt@reqres.in",
  password: "pistol",
};
const incorrectemail = {
  email: "sydney@fife",
};
const correctlogin = {
  email: "eve.holt@reqres.in",
  password: "cityslicka",
};
const incorrectlogin = {
  email: "peter@klaven",
};

Given("Estoy en la página de Reqres", () => {
  cy.visit("/");
});

When("Hago un Get a la lista de usuarios", () => {
  cy.request("GET", url + "/api/users?page=2").then((response) => {
    cy.log("request", response);
    expect(response.status).to.eq(200);
    expect(response.body).to.have.property("data");
  });
});

When("Hago un Get a la lista de usuarios unknown", () => {
  cy.request("GET", url + "/api/unknown").then((response) => {
    cy.log("request", response);
    expect(response.status).to.eq(200);
    expect(response.body).to.have.property("data");
  });
});

When("Hago un Post al crear un nuevo usuario", () => {
  cy.request("POST", url + "/api/users", newuser).then((response) => {
    cy.log("request", response);
    expect(response.status).to.eq(201);
    expect(response.body).to.have.property("id");
  });
});

When("Hago un Put al editar un usuario", () => {
  cy.request("PUT", url + "/api/users/2", updateuser).then((response) => {
    cy.log("request", response);
    expect(response.status).to.eq(200);
    expect(response.body.job).to.eql("zion resident");
  });
});

When("Hago un Patch al editar un usuario", () => {
  cy.request("PATCH", url + "/api/users/2", updateuser).then((response) => {
    cy.log("request", response);
    expect(response.status).to.eq(200);
    expect(response.body.job).to.eql("zion resident");
  });
});

When("Hago un Delete para borrar un usuario", () => {
  cy.request("DELETE", url + "/api/users/2").then((response) => {
    cy.log("request", response);
    expect(response.status).to.eq(204);
    expect(response.body).to.be.empty;
  });
});

When("Hago un Post para hacer un Register successful", () => {
  cy.request("POST", url + "/api/register", correctemail).then((response) => {
    cy.log("request", response);
    expect(response.status).to.eq(200);
    expect(response.body).to.have.property("token");
  });
});

When("Hago un Post para hacer un Register unsuccessful", () => {
  cy.request({
    method: "POST",
    url: url + "/api/register",
    failOnStatusCode: false,
    body: incorrectemail,
  }).then((response) => {
    cy.log("request", response);
    expect(response.status).to.eq(400);
    expect(response.body.error).to.eql("Missing password");
  });
});

When("Hago un Post para hacer un Login successful", () => {
  cy.request("POST", url + "/api/login", correctlogin).then((response) => {
    cy.log("request", response);
    expect(response.status).to.eq(200);
    expect(response.body).to.have.property("token");
  });
});

When("Hago un Post para hacer un Login insuccessful", () => {
  cy.request({
    method: "POST",
    url: url + "/api/login",
    failOnStatusCode: false,
    body: incorrectlogin,
  }).then((response) => {
    cy.log("request", response);
    expect(response.status).to.eq(400);
    expect(response.body.error).to.eql("Missing password");
  });
});
