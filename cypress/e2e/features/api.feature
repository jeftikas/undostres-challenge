Feature: Testear la api de Reqres 

    Background:
        Given Estoy en la página de Reqres

    Scenario: Probando los metodos http de la api de Reqres
        When Hago un Get a la lista de usuarios
        And Hago un Get a la lista de usuarios unknown
        And Hago un Post al crear un nuevo usuario
        And Hago un Put al editar un usuario
        And Hago un Patch al editar un usuario
        And Hago un Delete para borrar un usuario
        And Hago un Post para hacer un Register successful
        And Hago un Post para hacer un Register unsuccessful
        And Hago un Post para hacer un Login successful
        And Hago un Post para hacer un Login insuccessful 